# CSE411
# Tasmin Chowdhury - 1005025
# Arif Uz Zaman - 1005031
# Anika Tabassum - 1005041

'''
# For Human :

	$ Typical Cell Life Cycle Time 24 Hours
		- Growth Stage - 11 Hours
		- Replication Stage - 8 Hours
		- Mitosis Division Stage - 5 Hours

# Life Cycle Stage:

	G0 - 1st All Present Cell Start to Grow
	W1 - After Growth, All Cell Waits Some Times
	R0 - Now All Cell Start to Replicate it's DNA
	W2 - After Replication 2nd Waiting Periods Begun
	M0 - Finally Mitosis Cell Division Takes Place

# Growth >>> Wait >>> Replicate >>> Wait >>> Divide >>> Repeat

# In Simulation:

	$ Three Server
		- Growth
		- Replication
		- Division

	$ Only One Server Active at a Time
	$ Three Queue For Three Server
	$ Time Simulation Time = (Growth+Replication+Division) Server Time

	$ Random Distribution
		- Growth Server - Exponential (beta = .091, as lamda = 11)
		- Replication Server - Normal (mu = 8)
		- Division - Normal (mu = 5)

	$ In Human Body There Are Infinite Server, But Here in Simulation
		- Growth Server Count : Infinite
		- Replication Server Count : Finite
		- Division Server Count : Finite

	$ Priority Queue
		- Growth
			- Standar Queue > FCFS : First Come Frist Serve
		- Replication and Division
			- Heap Queue

Source:
	$ Wikipedia
	$ Bio-Informatics Related Sites

'''

# python 2.7.6

import heapq
import random
import matplotlib.pyplot as plt

cellLimit  = 0
simClock = 0

growthQueue = [] # Cell Growth Queue
replicationQueue = [] # Replication Server Queue
divisionQueue = [] # Division Server Queue

# Server Count
totalReplicationServer = 0
totalDivisionServer = 0

# Mean,Variance
beta = 0.091
mu_Replicate = 8
mu_Division = 5
sigma = 1

# Results
# gs - Growth Server
# rs - Replication Server
# ds - Division Server
gsServiceTime,rsWaitingTime,rsServiceTime,dsWaitingTime,dsServiceTime = [],[],[],[],[]
growthServerUse,replicationServerUse,divisionServerUse = [],[],[]
divided,dead = 0,0


# Here Cell Growth Started
# Cell Growth Time Follows Exponential Distribution

class Growth:
	def __init__(self):
		self.run()
	
	def run(self):
		global simClock
		temp = []
		while True:
			if len(growthQueue) < cellLimit:
				if len(growthQueue) == 0:
					growthQueue.append(1)
					#print "1st Cell is Created!!\n"

				while len(growthQueue) > 0:
					cellNumber = growthQueue.pop(0)
					#print "Cell %d has Started to Grow at %d" % (cellNumber,simClock)

					growthTime = random.expovariate(0.091)
					gsServiceTime.append(growthTime)
					joinQueue = simClock+growthTime
					replicationQueue.append(joinQueue)
					temp.append(growthTime)
					#print "Cell %d Completes it's Growth at %d" % (cellNumber,simClock)
					#print "Cell %d Joins Replication Server Queue at %d" % (cellNumber,simClock)

				#print gsServiceTime
				#print replicationQueue
				heapq.heapify(replicationQueue)
				#print replicationQueue
				#print
				high = max(temp) # Maximum Growth time
				# As Infinite in Size, Growth Server remain active till maximum Growth time for this instance
				simClock = simClock+high
				growthServerUse.append(high)
				new_Replication = Replication()
			else:
				break


# When Growth Completes Cell Joins to Replication Server Queue
# Here Cell Waits for Replication Server to Become Free
class Replication:
	def __init__(self):
		self.run()


	def run(self):
		global simClock
		# a - arrival, ss - service start, w - wait, s - service, se - service end, ts - total service
		aTime = ssTime = wTime = sTime = seTime = tsTime = 0
		start,end = 0,0
		# Keeping Track of Service End Time
		last_k_serviceEndTime = []

		N = len(replicationQueue)
		#print N
		if N > 0:
			# All Cell Waited Till SimClock. at Simclock Replication Server become active
			start = simClock
			if N<=totalReplicationServer:
				for x in xrange(N):
					aTime = replicationQueue.pop(0)
					ssTime = max(simClock,aTime)
					wTime = ssTime - aTime
					sTime = self.server(ssTime)
					seTime = ssTime+sTime
					end = max(end,seTime)
					rsWaitingTime.append(wTime)
					rsServiceTime.append(sTime)
			else:
				for x in xrange(totalReplicationServer):
					aTime = replicationQueue.pop(0)
					ssTime = max(simClock,aTime)
					wTime = ssTime - aTime
					sTime = self.server(ssTime)
					seTime = aTime+sTime
					end = max(end,seTime)
					rsWaitingTime.append(wTime)
					rsServiceTime.append(sTime)
					last_k_serviceEndTime.append(seTime)

				while len(replicationQueue) > 0:
					aTime = replicationQueue.pop(0)
					'''
					min(last_k_serviceEndTime) - when, atleast one server will become Free
					if arrival time > min(last_k_serviceEndTime)
						then atleast one server free, jonis immediately
					else
						server busy, wait for server to become free
					'''
					ssTime = max(simClock,aTime,min(last_k_serviceEndTime))
					wTime = ssTime - aTime
					sTime = self.server(ssTime)
					seTime = ssTime+sTime
					end = max(end,seTime)
					rsWaitingTime.append(wTime)
					rsServiceTime.append(sTime)

					# 1st server to become free, will be taken, so remove it from free list
					last_k_serviceEndTime.sort()
					last_k_serviceEndTime.pop(0)

					# now, append when acquired server will become free
					last_k_serviceEndTime.append(seTime)

		#print rsWaitingTime
		#print rsServiceTime
		#print

		# now, server remain till last cell service end time
		tsTime = end-start
		replicationServerUse.append(tsTime)
		#print divisionQueue
		heapq.heapify(divisionQueue)
		#print replicationQueue
		#print
		simClock += tsTime
		new_Division = Division()


	# Replication Server - Here DNA Replication Occurs.
	# DNA Replication Time Follows Normal Distribution
	def server(self,servStartTime):
		random_num = random.normalvariate(mu_Replicate,sigma**2)
		serviceTime = abs(random_num)
		temp = servStartTime+serviceTime
		divisionQueue.append(temp)
		return serviceTime


# When DNA Replication Completes Cell Joins to Division Server Queue
# Here Cell Waits for Division Server to Become Free
class Division:
	def __init__(self):
		self.run()

	def run(self):
		global simClock
		# a - arrival, ss - service start, w - wait, s - service, se - service end, ts - total service
		aTime = ssTime = wTime = sTime = seTime = tsTime = 0
		start,end = 0,0
		# Keeping Track of Service End Time
		last_k_serviceEndTime = []

		N = len(divisionQueue)
		#print N
		if N > 0:
			# All Cell Waited Till SimClock. at Simclock Division Server become active
			start = simClock
			if N<=totalDivisionServer:
				for x in xrange(N):
					aTime = divisionQueue.pop(0)
					ssTime = max(simClock,aTime)
					wTime = ssTime - aTime
					sTime = self.server()
					seTime = ssTime+sTime
					end = max(end,ssTime)
					dsWaitingTime.append(wTime)
					dsServiceTime.append(sTime)
			else:
				for x in xrange(totalDivisionServer):
					aTime = divisionQueue.pop(0)
					ssTime = max(simClock,aTime)
					wTime = simClock - aTime
					sTime = self.server()
					seTime = ssTime+sTime
					end = max(end,seTime)
					dsWaitingTime.append(wTime)
					dsServiceTime.append(sTime)
					last_k_serviceEndTime.append(seTime)

				while len(divisionQueue) > 0:
					aTime = divisionQueue.pop(0)
					'''
					min(last_k_serviceEndTime) - when atleast one server will become Free
					if arrival time > min(last_k_serviceEndTime)
						then atleast one server free, jonis immediately
					else
						server busy, wait for server to become free
					'''
					ssTime = max(simClock,aTime,min(last_k_serviceEndTime))
					wTime = ssTime - aTime
					sTime = self.server()
					seTime = ssTime+sTime
					end = max(end,seTime)
					dsWaitingTime.append(wTime)
					dsServiceTime.append(sTime)

					# 1st server to become free, will be taken, so remove it from free list
					last_k_serviceEndTime.sort()
					last_k_serviceEndTime.pop(0)

					# now, append when acquired server will become free
					last_k_serviceEndTime.append(seTime)

		# now, server remain till last cell service end time
		tsTime = end-start
		divisionServerUse.append(tsTime)
		simClock += tsTime
		# Growth Queue : FCFS - First Come First Serve
		growthQueue.sort()


	# Division Server - Here Cell Divides itself into Two New Cells
	# Service Time Follows Normal Distribution
	def server(self):
		global divided,dead

		# random.gauss - More Efficient Implementation of Normal Distribution
		random_num = random.normalvariate(mu_Division,sigma**2)
		serviceTime = abs(random_num)

		if len(growthQueue) == 0:
			num = len(gsServiceTime)
		else:
			num = growthQueue[-1]

		#print "Cell %d Completes it's Division at %d and Created Two New Cell %d and %d" % (cellNumber,simClock,num+1,num+2)
		divided = divided+1

		if self.alive():
			if len(growthQueue)>=cellLimit:
				dead = dead+1
			else:
				growthQueue.append(num+1)
				#print "Yeah!! Cell %d is Alive and Joined Cell Growth Queue" % (num+1)
		else:
			dead = dead+1
			#print "Alas!! Cell %d Has Been Died." % (num+1)

		if self.alive():
			if len(growthQueue)>=cellLimit:
				dead = dead+1
			else:
				growthQueue.append(num+2)
				#print "Yeah!! Cell %d is Alive and Joined Cell Growth Queue" % (num+2)
		else:
			dead = dead+1
			#print "Alas!! Cell %d Has Been Died." % (num+2)

		#print "Total Cell Present : %d\n" % len(growthQueue)
		return serviceTime


	# Checks if Newly Created Cell is Dead or Alive
	def alive(self):
		num = random.randint(1,3)
		if num == 3:
			return False
		else:
			return True


class Result:
	def __init__(self):
		self.showResult()
		
	# Start The Simulation
	def showResult(self):
		while True:
			print
			print "View Results Option :"
			print "\t1 - Generate Statistics"
			print "\t2 - plot 7,8,9 : Server Service Time"
			print "\t3 - plot1 : Average Waiting Time Vs Server Count"
			print "\t4 - plot2 : Server Utilization Vs Server Count"
			print "\t5 - plot6 : Impact Of Seed Changes : Divided and Dead Cell"
			print "\t6 - plot3 : Impact Of Seed Changes : Growth Server"
			print "\t7 - plot4 : Impact Of Seed Changes : Replication Server"
			print "\t8 - plot5 : Impact Of Seed Changes : Division Server"
			print "\t0 - Exit"

			a = int(raw_input("Enter a Choice (0-8) : "))

			if a==0:
				break

			elif a==1:
				self.printResult()
				
			elif a==2:
				self.plot7()
				self.plot8()
				self.plot9()

			elif a==3:
				self.plot1()

			elif a==4:
				self.plot2()

			elif a==5:
				self.plot6()

			elif a==6:
				self.plot3()

			elif a==7:
				self.plot4()

			elif a==8:
				self.plot5()
		print
		
		
	# Reset All Value, Prepare for New Simulation
	def reset(self,limit,dna,mitosis):
		global cellLimit,totalReplicationServer,totalDivisionServer
		global growthQueue,replicationQueue,divisionQueue
		global gsServiceTime,rsWaitingTime,rsServiceTime,dsWaitingTime,dsServiceTime
		global growthServerUse,replicationServerUse,divisionServerUse
		global divided,dead,simClock

		cellLimit,totalReplicationServer,totalDivisionServer = limit,dna,mitosis
		growthQueue,replicationQueue,divisionQueue = [],[],[]
		gsServiceTime,rsWaitingTime,rsServiceTime,dsWaitingTime,dsServiceTime = [],[],[],[],[]
		growthServerUse,replicationServerUse,divisionServerUse = [],[],[]
		simClock,divided,dead = 0,0,0


	# Run Simulation
	def run(self,limit,dna,mitosis):
		self.reset(limit,dna,mitosis)
		#print len(growthQueue),len(replicationQueue),len(divisionQueue)
		#print sum(rsWaitingTime),sum(dsWaitingTime)
		#print sum(growthServerUse),sum(replicationServerUse),sum(divisionServerUse)
		new_Simu = Growth()
		
		#print len(gsServiceTime),len(rsWaitingTime),len(rsServiceTime),len(dsWaitingTime),len(dsServiceTime)


	# Printing Result
	def printResult(self):
		random.seed(100)
		self.run(100,20,10)
		print "\nAs Present Cell Count Hits %d, Simulation Stop.\n" % cellLimit
		Avg_gsServiceTime = sum(gsServiceTime) / float(len(gsServiceTime))
		Avg_rsWaitingTime = sum(rsWaitingTime) / float(len(rsWaitingTime)) / totalReplicationServer
		Avg_dsWaitingTime = sum(dsWaitingTime) / float(len(dsWaitingTime)) / totalDivisionServer
		Avg_rsServiceTime = sum(rsServiceTime) / float(len(rsServiceTime))
		Avg_dsServiceTime = sum(dsServiceTime) / float(len(dsServiceTime))
		Avg_lt = Avg_gsServiceTime+Avg_rsServiceTime+Avg_dsServiceTime
		gsUtility = (sum(growthServerUse) / float(simClock)) * 100
		dsUtility = (sum(replicationServerUse) / float(simClock)) * 100
		msUtility = (sum(divisionServerUse) / float(simClock)) * 100

		print "**** Simulation Statistics ****"
		print
		print "beta = %5.2f \t mu_Replicate = %5.2f \t mu_Division = %5.2f \t sigma = %5.2f" % (beta,mu_Replicate,mu_Division,sigma)
		print "Replication Server = %d \t Division Server = %d" % (totalReplicationServer,totalDivisionServer)
		print
		print "Total Cell Divided : %d" % divided
		print "Total Dead Cell : %d" % dead
		print "Currently Alive Cell : %d" % len(growthQueue)
		print
		print "Average Cell Growth Time : %5.2f hours" % Avg_gsServiceTime
		print "Average Replication Server Queue Waiting Time : %5.2f hours" % Avg_rsWaitingTime
		print "Average Replication Server Service Time : %5.2f hours" % Avg_rsServiceTime
		print "Average Division Server Queue Waiting Time : %5.2f hours" % Avg_dsWaitingTime
		print "Average Division Server Service Time : %5.2f hours" % Avg_dsServiceTime
		print "Average Life Time of a Cell : %5.2f hours" % Avg_lt
		print
		print "Total Simulation Time : %5d hours" % simClock
		print "Total Growth Server Service Time : %5d hours" % sum(growthServerUse)
		print "Growth Server Utilization : %5.2f%s" % (gsUtility,"%")
		print "Total Replication Server Service Time : %5d hours" % sum(replicationServerUse)
		print "Replication Server Utilization : %5.2f%s" % (dsUtility,"%")
		print "Total Division Server Service Time : %5d hours" % sum(divisionServerUse)
		print "Division Server Utilization : %5.2f%s" % (msUtility,"%")


	# Plotting Results
	def getXY(self,temp):
		temp2,temp3 = [],[]
		minimum = min(temp)
		maximum = max(temp)
		print minimum,maximum
		rangeT = maximum-minimum
		add = rangeT/24.0

		for x in xrange(25):
			temp2.append(minimum+x*add)

		for x in xrange(24):
			count = 0
			for y in xrange(len(temp)):
				if temp[y]>=temp2[x] and temp[y]<temp2[x+1]:
					count = count+1
			temp3.append(count)

		temp3.append(len(temp)-sum(temp3))
		return temp2,temp3


	def plot1(self):
		global totalReplicationServer,totalDivisionServer
		random.seed(100)
		X,Y1,Y2 = [],[],[]

		for x in xrange(1,11):
			totalReplicationServer = x
			totalDivisionServer = x
			self.run(100,totalReplicationServer,totalDivisionServer)
			Avg_rsWaitingTime = sum(rsWaitingTime) / float(len(rsWaitingTime)) / totalReplicationServer
			Avg_dsWaitingTime = sum(dsWaitingTime) / float(len(dsWaitingTime)) / totalDivisionServer
			X.append(x)
			Y1.append(Avg_rsWaitingTime)
			Y2.append(Avg_dsWaitingTime)

		plt.figure(1)
		dna, = plt.plot(X,Y1,'r-',label='dnaWaitQueue')
		mitosis, = plt.plot(X,Y2,'g-',label='msWaitQueue')
		plt.legend([dna, mitosis], ['Avg Replication Server Queue Waiting Time', 'Avg Division Server Queue Waiting Time'])
		plt.suptitle("Average Waiting Time Vs Server Count", fontsize=16)
		plt.xlabel("Server Count")
		plt.ylabel("Waiting Time (hours)")
		plt.show()


	def plot2(self):
		global totalReplicationServer,totalDivisionServer
		random.seed(100)
		X,Y1,Y2,Y3 = [],[],[],[]

		for x in xrange(1,11):
			totalReplicationServer = x
			totalDivisionServer = x
			self.run(100,totalReplicationServer,totalDivisionServer)
			gsUtility = (sum(growthServerUse) / float(simClock)) * 100
			dsUtility = (sum(replicationServerUse) / float(simClock)) * 100
			msUtility = (sum(divisionServerUse) / float(simClock)) * 100
			
			X.append(x)
			Y1.append(gsUtility)
			Y2.append(dsUtility)
			Y3.append(msUtility)

		plt.figure(2)
		growth, = plt.plot(X,Y1,'r-',label='GrowthServiceTime')
		dna, = plt.plot(X,Y2,'g-',label='msServiceTime')
		mitosis, = plt.plot(X,Y3,'b-',label='msServiceTime')
		plt.legend([growth, dna, mitosis], ['Growth Server','Replication Server', 'Division Server'])
		plt.suptitle("Server Utilization Vs Server Count", fontsize=16)
		plt.xlabel("Server Count")
		plt.ylabel("Server Utilization (%)")
		plt.show()


	def plot3(self):
		global totalReplicationServer,totalDivisionServer

		X,Y1,Y2,Y3 = [],[],[],[]

		random.seed(100)
		for x in xrange(1,11):
			totalReplicationServer = x
			totalDivisionServer = x
			self.run(100,totalReplicationServer,totalDivisionServer)
			Avg_gsServiceTime = sum(gsServiceTime) / float(len(gsServiceTime))
			X.append(x)
			Y1.append(Avg_gsServiceTime)

		random.seed(110)
		for x in xrange(1,11):
			totalReplicationServer = x
			totalDivisionServer = x
			self.run(100,totalReplicationServer,totalDivisionServer)
			Avg_gsServiceTime = sum(gsServiceTime) / float(len(gsServiceTime))
			Y2.append(Avg_gsServiceTime)

		for x in xrange(10):
			Y3.append(abs(Y1[x]-Y2[x]))

		plt.figure(3)
		seed_100, = plt.plot(X,Y1,'r-',label='SEED_100')
		seed_110, = plt.plot(X,Y2,'g-',label='SEED_110')
		variation, = plt.plot(X,Y3,'b-',label='VARIATION')
		plt.legend([seed_100, seed_110, variation], ['@SEED_100','@SEED_110', 'VARIATION'])
		plt.suptitle("Impact Of Seed Changes : Growth Server", fontsize=16)
		plt.xlabel("Server Count")
		plt.ylabel("Service Time (hours)")
		plt.show()


	def plot4(self):
		global totalReplicationServer,totalDivisionServer
		X,Y1,Y2,Y3,Y4,Y5,Y6 = [],[],[],[],[],[],[]

		random.seed(100)
		for x in xrange(1,11):
			totalReplicationServer = x
			totalDivisionServer = x
			self.run(100,totalReplicationServer,totalDivisionServer)
			Avg_rsWaitingTime = sum(rsWaitingTime) / float(len(rsWaitingTime)) / totalReplicationServer
			Avg_rsServiceTime = sum(rsServiceTime) / float(len(rsServiceTime))
			X.append(x)
			Y1.append(Avg_rsWaitingTime)
			Y4.append(Avg_rsServiceTime)

		random.seed(110)
		for x in xrange(1,11):
			totalReplicationServer = x
			totalDivisionServer = x
			self.run(100,totalReplicationServer,totalDivisionServer)
			Avg_rsWaitingTime = sum(rsWaitingTime) / float(len(rsWaitingTime)) / totalReplicationServer
			Avg_rsServiceTime = sum(rsServiceTime) / float(len(rsServiceTime))
			Y2.append(Avg_rsWaitingTime)
			Y5.append(Avg_rsServiceTime)

		for x in xrange(10):
			Y3.append(abs(Y1[x]-Y2[x]))
			Y6.append(abs(Y4[x]-Y5[x]))

		plt.figure(4)
		plt.subplot(211)
		#seed_100, = plt.plot(X,Y1,'r-',label='SEED_100')
		#seed_110, = plt.plot(X,Y2,'b-',label='SEED_110')
		variation, = plt.plot(X,Y3,'g-',label='VARIATION')
		plt.legend([variation], ['Variation Between Seed 110 and Seed 100'])
		plt.suptitle("Impact Of Seed Changes : Replication Server", fontsize=16)
		plt.xlabel("Server Count")
		plt.ylabel("Average Waiting Time (hours)")

		plt.subplot(212)
		#seed_100, = plt.plot(X,Y4,'r-',label='SEED_100')
		#seed_110, = plt.plot(X,Y5,'b-',label='SEED_110')
		variation, = plt.plot(X,Y6,'g-',label='VARIATION')
		plt.xlabel("Server Count")
		plt.ylabel("Average Service Time (hours)")
		plt.show()


	def plot5(self):
		global totalReplicationServer,totalDivisionServer
		X,Y1,Y2,Y3,Y4,Y5,Y6 = [],[],[],[],[],[],[]

		random.seed(100)
		for x in xrange(1,11):
			totalReplicationServer = x
			totalDivisionServer = x
			self.run(100,totalReplicationServer,totalDivisionServer)
			Avg_dsWaitingTime = sum(dsWaitingTime) / float(len(dsWaitingTime)) / totalDivisionServer
			Avg_dsServiceTime = sum(dsServiceTime) / float(len(dsServiceTime))
			X.append(x)
			Y1.append(Avg_dsWaitingTime)
			Y4.append(Avg_dsServiceTime)

		random.seed(110)
		for x in xrange(1,11):
			totalReplicationServer = x
			totalDivisionServer = x
			self.run(100,totalReplicationServer,totalDivisionServer)
			Avg_dsWaitingTime = sum(dsWaitingTime) / float(len(dsWaitingTime)) / totalDivisionServer
			Avg_dsServiceTime = sum(dsServiceTime) / float(len(dsServiceTime))
			Y2.append(Avg_dsWaitingTime)
			Y5.append(Avg_dsServiceTime)

		for x in xrange(10):
			Y3.append(abs(Y1[x]-Y2[x]))
			Y6.append(abs(Y4[x]-Y5[x]))

		plt.figure(5)
		plt.subplot(211)
		#seed_100, = plt.plot(X,Y1,'r-',label='SEED_100')
		#seed_110, = plt.plot(X,Y2,'g-',label='SEED_110')
		variation, = plt.plot(X,Y3,'b-',label='VARIATION')
		plt.legend([variation], ['Variation Between Seed 110 and Seed 100'])
		plt.suptitle("Impact Of Seed Changes : Division Server", fontsize=16)
		plt.xlabel("Server Count")
		plt.ylabel("Average Waiting Time (hours)")

		plt.subplot(212)
		#seed_100, = plt.plot(X,Y4,'r-',label='SEED_100')
		#seed_110, = plt.plot(X,Y5,'g-',label='SEED_110')
		variation, = plt.plot(X,Y6,'b-',label='VARIATION')
		plt.xlabel("Server Count")
		plt.ylabel("Average Service Time (hours)")
		plt.show()


	def plot6(self):

		X,Y1,Y2 = [],[],[]

		for x in xrange(10):
			random.seed(x*10)
			self.run(100,5,5)
			X.append(x*10)
			Y1.append(divided)
			Y2.append(dead)

		plt.figure(6)
		divide, = plt.plot(X,Y1,'r-',label='Divided')
		dead1, = plt.plot(X,Y2,'g-',label='Dead')
		plt.legend([divide, dead1], ['Divided','Dead'])
		plt.suptitle("Impact Of Seed Changes : Divided and Dead Cell", fontsize=16)
		plt.xlabel("Seed (Please Note : Divided-Dead != Alive)")
		plt.ylabel("Total")
		plt.show()
		
	
	def plot7(self):
		random.seed(100)
		X,Y = [],[]
		
		self.run(100,20,12)
		X,Y = self.getXY(gsServiceTime)

		plt.figure(7)
		growth, = plt.plot(X,Y,'g-')
		plt.suptitle("Growth Server Service Time", fontsize=16)
		plt.ylabel("Number of Service")
		plt.xlabel("Bins")
		plt.show()


	def plot8(self):
		random.seed(5)
		X1,Y1 = [],[]
		
		self.run(100,20,12)
		X,Y = self.getXY(rsServiceTime)

		plt.figure(8)
		replicate, = plt.plot(X,Y,'g-')
		plt.suptitle("Replication Server Service Time", fontsize=16)
		plt.ylabel("Number of Service")
		plt.xlabel("Bins")
		plt.show()


	def plot9(self):
		random.seed(100)
		X,Y = [],[]
		
		self.run(100,20,12)
		X,Y = self.getXY(dsServiceTime)

		plt.figure(9)
		division, = plt.plot(X,Y,'g-')
		plt.suptitle("Division Server Service Time", fontsize=16)
		plt.ylabel("Number of Service")
		plt.xlabel("Bins")
		plt.show()


if __name__ == "__main__":
	new_Simulation = Result()
